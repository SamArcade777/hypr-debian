#!/bin/bash

kitty=(
"kitty_dracula"
)


# kitty dracula theme Installation


kitty_dracula_install() {
   if ls "$HOME/.config/kitty/kitty.conf" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
   fi
}



# Installing the kitty_dracula theme


printf "\n%s - Installing $kitty .... \n"


for kitty_theme in "$kitty"; do
    echo "Installing $kitty_theme..."
    kitty_dracula_install "$kitty_theme"
done


echo -e "$kitty is installed successfully..."
exit 1
