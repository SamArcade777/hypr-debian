#!/bin/bash

fuzzel=(
"fuzzel_dracula"
)


# fuzzel dracula theme Installation


fuzzel_dracula_install() {
   if ls "$HOME/.config/fuzzel/fuzzel.ini" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
   fi
}



# Installing the fuzzel_dracula theme


printf "\n%s - Installing $fuzzel .... \n"


for fuzzel_theme in "$fuzzel"; do
    echo "Installing $fuzzel_theme..."
    fuzzel_dracula_install "$fuzzel_theme"
done


echo -e "$fuzzel is installed successfully..."
exit 1
