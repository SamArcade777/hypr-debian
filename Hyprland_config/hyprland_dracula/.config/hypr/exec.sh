#!/bin/bash

# initializing the wallpaper daemon
#swww init --no-daemon &
swww query || swww init &

# Waybar
waybar &

# mako
mako &

# startup applications
nm-applet --indicator &
