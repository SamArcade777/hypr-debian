#!/bin/bash

wpctl get-volume @DEFAULT_AUDIO_SINK@ | \
    sed 's/Volume: //' | \
    xargs -I {} zsh -c 'qalc -t -s "decimal comma off" "{} * 100"'
