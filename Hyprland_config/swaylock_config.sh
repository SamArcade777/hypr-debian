#!/bin/bash

swaylock=(
"swaylock_dracula"
)


# swaylock dracula theme Installation


swaylock_dracula_install() {
   if ls "$HOME/.config/swaylock/config" &> /dev/null; then
     echo -e "$1 theme is already installed"
   else
     echo -e "$1 theme is not installed"
     cp -r "$1/.config/." "$HOME/.config/"
   fi
}



# Installing the swaylock_dracula theme


printf "\n%s - Installing $swaylock .... \n"


for swaylock_theme in "$swaylock"; do
    echo "Installing $swaylock_theme..."
    swaylock_dracula_install "$swaylock_theme"
done


echo -e "$swaylock is installed successfully..."
exit 1
