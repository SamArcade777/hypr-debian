<div align="center">
    
# Hypr-Debian
<br/>
</div>

<p align="center" width="100%">
    <img align="center" width="50%" src="https://gitlab.com/SamArcade777/Hypr-Debian/-/raw/main/assets/Hypr-Debian_1.png" />
</p>


# Hypr-Debian is a project to run Hyprland on Debian

| OS Support         | Kernel Version | Hyprland Version Support |
|--------------------|----------------|--------------------------|
| Debian 12 (Stable) | 6.1.0          | V0.29.1                  |


## Video Tutorial
[![Hypr-Debian](https://img.youtube.com/vi/SiLXXv7URAg/maxresdefault.jpg)](https://www.youtube.com/watch?v=SiLXXv7URAg)

## Imp info

- Do not run the installation scripts as sudo or root.
- Make sure to edit the sources.list before the installation of Hypr-Debian.
```bash
sudo nano /etc/apt/sources.list
```
``` bash
deb https://deb.debian.org/debian/ bookworm main non-free-firmware contrib non-free
deb-src https://deb.debian.org/debian/ bookworm main non-free-firmware contrib non-free

deb https://security.debian.org/debian-security bookworm-security main non-free-firmware contrib non-free
deb-src https://security.debian.org/debian-security bookworm-security main non-free-firmware contrib non-free

deb https://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib non-free
deb-src https://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib non-free
```
- Everything is pre-compiled, only thing you have to do is to run these three scripts.
``` bash
./Hypr-install.sh
./tools_install.sh
./config_install.sh
```

  ### Important NOTICE
  Due to stable nature of Debian, the latest version of Hyprland latest version is only available on the Debian unstable (sid) or Debian 13 Trixie.

# Some Screenshots:-

![](https://gitlab.com/SamArcade777/Hypr-Debian/-/raw/main/assets/img_1.png)

![](https://gitlab.com/SamArcade777/Hypr-Debian/-/raw/main/assets/img_2.png)

![](https://gitlab.com/SamArcade777/Hypr-Debian/-/raw/main/assets/img_3.png)

# Special Thanks
- Thanks @vaxerski for the [Hyprland](https://hyprland.org/) (amazing Dynamic Tiling Manager).

## Support
- Subscribe to my [Youtube](https://www.youtube.com/@samarcade) Channel.
- If you like my project then give my Gitlab repo a star.