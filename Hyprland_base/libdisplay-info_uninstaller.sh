#!/bin/bash

libdisplay_directories=(
  "/usr/bin/di-edid-decode"
  "/usr/include/libdisplay-info"
  "/usr/lib/x86_64-linux-gnu/libdisplay-info.so"
  "/usr/lib/x86_64-linux-gnu/libdisplay-info.so.0.1.1"
  "/usr/lib/x86_64-linux-gnu/libdisplay-info.so.1"
  "/usr/lib/x86_64-linux-gnu/pkgconfig/libdisplay-info.pc"
)

rm_libdisplay() {
    for dir in "${libdisplay_directories[@]}"; do
        if [ "$dir" ]; then
            sudo rm -rf "$dir"
            echo "Removed: $dir"
        fi
    done
}

read -p "Do you want to uninstall libdisplay-info? (yes/no): " ans

answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')

if [ "$answer" = "yes" ]; then
    rm_libdisplay
    echo "Libdisplay-info directories removed."
elif [ "$answer" = "no" ]; then
    echo "Uninstallation canceled"
else
    echo "Invalid option. Please enter 'yes' or 'no' "
fi
