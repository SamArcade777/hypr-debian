#!/bin/bash

libliftoff_Depends=(
libdrm2
)


libliftoff=(
"libliftoff-0.5.0"
)


# libinput Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies


printf "\n%s - Installing dependencies.... \n"


for pkg in "${libliftoff_Depends[@]}"; do
  pkg_install "$pkg"
done



# libliftoff V0.5.0 Installation


libliftoff_install() {
   if ls "/usr/lib/x86_64-linux-gnu/libliftoff.so" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the libliftoff-0.5.0


printf "\n%s - Installing $libliftoff .... \n"


for libliftoff_pkg in "$libliftoff"; do
    echo "Installing $libliftoff_pkg..."
    libliftoff_install "$libliftoff_pkg"
done


echo -e "$libliftoff is installed successfully..."
exit 1
