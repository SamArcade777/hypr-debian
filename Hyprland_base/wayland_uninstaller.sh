#!/bin/bash

wayland_directories=(
  "/usr/bin/wayland-scanner"
  /usr/include/wayland-*.h
  /usr/lib/x86_64-linux-gnu/libwayland-*
  "/usr/share/aclocal/wayland-scanner.m4"
  /usr/share/pkgconfig/wayland-c*.pc
  /usr/share/pkgconfig/wayland-e*.pc
  /usr/share/pkgconfig/wayland-s*.pc
  /usr/share/wayland/wayland*
)

rm_wayland() {
    for dir in "${wayland_directories[@]}"; do
        if [ "$dir" ]; then
            sudo rm -rf "$dir"
            echo "Removed: $dir"
        fi
    done
}

read -p "Do you want to uninstall wayland? (yes/no): " ans

answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')

if [ "$answer" = "yes" ]; then
    rm_wayland
    echo "Wayland directories removed."
elif [ "$answer" = "no" ]; then
    echo "Uninstallation canceled"
else
    echo "Invalid option. Please enter 'yes' or 'no' "
fi
