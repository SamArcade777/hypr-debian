#!/bin/bash

libliftoff_directories=(
  /usr/lib/x86_64-linux-gnu/libliftoff*
  "/usr/lib/x86_64-linux-gnu/pkgconfig/libliftoff.pc"
  "/usr/include/libliftoff.h"
)

rm_libliftoff() {
    for dir in "${libliftoff_directories[@]}"; do
        if [ "$dir" ]; then
            sudo rm -rf "$dir"
            echo "Removed: $dir"
        fi
    done
}

read -p "Do you want to uninstall libliftoff? (yes/no): " ans

answer=$(echo "$ans" | tr '[:upper:]' '[:lower:]')

if [ "$answer" = "yes" ]; then
    rm_libliftoff
    echo "Libliftoff directories removed."
elif [ "$answer" = "no" ]; then
    echo "Uninstallation canceled"
else
    echo "Invalid option. Please enter 'yes' or 'no' "
fi
