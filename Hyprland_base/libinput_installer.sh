#!/bin/bash

libinput_Depends=(
libevdev2
libmtdev1
libudev1
libwacom9
libmtdev-dev
libudev-dev
libevdev-dev
libwacom-dev
)


libinput=(
"libinput_1.24.0_amd64"
)


# libinput Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies


printf "\n%s - Installing dependencies.... \n"


for pkg in "${libinput_Depends[@]}"; do
  pkg_install "$pkg"
done



# libinput v1.24.0 Installation


libinput_install() {
   if ls "/usr/bin/libinput" &> /dev/null; then
     echo -e "$libinput is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$libinput/usr/." "/usr"
     sudo cp -r "$libinput/lib/." "/lib"
   fi
}



# Installing the Libinput


printf "\n%s - Installing Libinput V1.24.0 .... \n"


for libinput_pkg in "$libinput"; do
    echo "Installing $libinput_pkg..."
    libinput_install "$libinput_pkg"
done


echo -e "libinput v1.24.0 is installed successfully..."
exit 1
