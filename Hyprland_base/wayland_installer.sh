#!/bin/bash

wayland_Depends=(
libxml2
libexpat1
libffi-dev
libffi8

)


wayland=(
"wayland-1.22.0"
)


# wayland Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies


printf "\n%s - Installing dependencies.... \n"


for pkg in "${libliftoff_Depends[@]}"; do
  pkg_install "$pkg"
done



# Wayland V1.22.0 Installation

wayland_install() {
   if "/usr/bin/./wayland-scanner" --version &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the wayland-1.22.0


printf "\n%s - Installing $wayland .... \n"


for wayland_pkg in "$wayland"; do
    echo "Installing $wayland_pkg..."
    wayland_install "$wayland_pkg"
done


echo -e "$wayland is installed successfully..."
exit 1
