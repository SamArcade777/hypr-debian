#!/bin/bash

packages=(
htop
thunar
git
curl
wget
fuzzel
gvfs
gvfs-backends
network-manager-gnome
pavucontrol
libnotify-bin
libnotify-dev
libnotify4
grim
slurp
wl-clipboard
wlogout
wlsunset
brightnessctl
cava
vim
bluez
blueman
libspa-0.2-bluetooth
fonts-font-awesome
fonts-noto-cjk
mousepad
mpv
gparted
ranger
pipewire
wireplumber
pipewire-pulse
pipewire-audio-client-libraries
libgtk-layer-shell-dev
libgtk-layer-shell0
clipman
firefox-esr
network-manager
network-manager-gnome
neofetch
ntfs-3g
exfat-fuse
speedometer
)

# Packages for Hyprland

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}

# Installation of the Packages

printf "\n%s - Installing dependencies.... \n"

for pkg in "${packages[@]}"; do
  pkg_install "$pkg"
done


printf "\n%s - pipewire & wireplumber.... \n"
systemctl --user mask pulseaudio
systemctl --user --now enable pipewire pipewire-pulse wireplumber
