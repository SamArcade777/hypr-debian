#!/bin/bash

waybar_Depends=(
libfmt9
libfmt-dev
libudev1
libstdc++6
libpulse0
libgtk-3-0
libgtk-layer-shell0
init-system-helpers
libatkmm-1.6-1v5
libcairomm-1.0-1v5
libdate-tz3
libdbusmenu-gtk3-4
libevdev2
libgcc-s1
libglibmm-2.4-1v5
libgtkmm-3.0-1v5
libmpdclient2
libnl-genl-3-200
libunwind8
libupower-glib3
libwireplumber-0.4-0
libxkbregistry0
fonts-font-awesome
)


waybar=(
"waybar-v0.9.24"
)


# libinput Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies


printf "\n%s - Installing dependencies.... \n"


for pkg in "${waybar_Depends[@]}"; do
  pkg_install "$pkg"
done



# waybar Installation


waybar_install() {
   if ls "/usr/bin/waybar" &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
     sudo cp -r "$1/etc/." "/etc"
   fi
}



# Installing the waybar


printf "\n%s - Installing $waybar .... \n"


for waybar_pkg in "$waybar"; do
    echo "Installing $waybar_pkg..."
    waybar_install "$waybar_pkg"
done


echo -e "$waybar is installed successfully..."
exit 1
