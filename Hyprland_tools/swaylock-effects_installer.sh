#!/bin/bash

swaylock_effects=(
"swaylock-effects-v1.6-4-2"
)


# swaylock-effects-v1.6-4-2 Installation


swaylock_effects_install() {
   if "/usr/local/bin/swaylock" --version &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
     sudo chmod a+s "/usr/local/bin/swaylock"
   fi
}



# Installing the swaylock-effects-v1.6-4-2


printf "\n%s --> Installing $swaylock_effects .... \n"


for swaylock_effects_pkg in "$swaylock_effects"; do
    echo "Installing $swaylock_effects_pkg ..."
    swaylock_effects_install "$swaylock_effects_pkg"
done


echo -e "$swaylock_effects is installed successfully..."
exit 1
