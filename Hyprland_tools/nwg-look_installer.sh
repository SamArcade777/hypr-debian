#!/bin/bash

nwg_look_Depends=(
libgtk-3-dev
libcairo2-dev
libglib2.0-bin
)


nwg_look=(
"nwg-look-v0.2.5"
)


# nwg-look Depends

pkg_install() {
 if sudo dpkg -s "$1" &> /dev/null ; then
   echo -e "$1 is already installed"
 else
   echo -e "Installing $1"
   sudo apt install -y "$1"
 fi
}


# Installation of the dependencies


printf "\n%s - Installing dependencies.... \n"


for pkg in "${nwg_look_Depends[@]}"; do
  pkg_install "$pkg"
done



# nwg-look Installation


nwg_look_install() {
   if "/usr/bin/./nwg-look" -v &> /dev/null; then
     echo -e "$1 is already installed"
   else
     echo -e "$1 pkg is not installed"
     sudo cp -r "$1/usr/." "/usr"
   fi
}



# Installing the nwg-look version 0.2.5


printf "\n%s - Installing $nwg_look .... \n"


for nwg_look_pkg in "$nwg_look"; do
    echo "Installing $nwg_look_pkg ..."
    nwg_look_install "$nwg_look_pkg"
done


echo -e "$nwg_look is installed successfully..."
exit 1
