#!/bin/bash

scripts=(
"extra_tools.sh"
"swww_installer.sh"
"waybar_installer.sh"
"nwg-look_installer.sh"
"swaylock-effects_installer.sh"
"sddm_installer.sh"
"mako_installer.sh"
)


# Hypr-Debian tools Installer

exec_script() {
 if [ -f "$1" ]; then
   echo -e "Launching $1 with sudo..."
   sudo bash "$1"
 else
   echo -e "Error: $1 not found...."
 fi
}

# exec scripts

printf "\n%s --> Installing .... \n"

for hypr in "${scripts[@]}"; do
  exec_script "$hypr"
done
